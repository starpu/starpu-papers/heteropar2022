#!/bin/bash

STARPU_NON_H_PATH=$(pwd)/starpu/non-h-install
STARPU_H_PATH=$(pwd)/starpu/h-install

CHAMELEON_NON_H_BUILD=$(pwd)/chameleon/non-h-build
CHAMELEON_H_BUILD=$(pwd)/chameleon/h-build

export STARPU_HOME=$(pwd)
export STARPU_SILENT=1
export STARPU_SCHED=dmdas
export STARPU_NCPU=30
export STARPU_NCUDA=2
export STARPU_CUDA_PIPELINE=4
