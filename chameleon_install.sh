#!/bin/bash

source env.sh

# Cloning Chameleon from tag papers/heteropar22
git clone --recursive --depth 1 --branch papers/heteropar22 https://gitlab.inria.fr/solverstack/chameleon.git
cd chameleon/

# Compilation
export PKG_CONFIG_PATH_SAVE=$PKG_CONFIG_PATH

## Non-Hierarchical Version
mkdir non-h-build
cd non-h-build
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH_SAVE:$STARPU_NON_H_PATH/lib/pkgconfig
cmake ..                              \
      -DCMAKE_BUILD_TYPE=Release      \
      -DCHAMELEON_USE_CUDA=ON         \
      -DCHAMELEON_USE_BUBBLE=OFF
make -j
cd ../

## Hierarchical Version
mkdir h-build
cd h-build
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH_SAVE:$STARPU_H_PATH/lib/pkgconfig
cmake ..                              \
      -DCMAKE_BUILD_TYPE=Release      \
      -DCHAMELEON_USE_CUDA=ON         \
      -DCHAMELEON_USE_BUBBLE=ON
make -j
cd ../
