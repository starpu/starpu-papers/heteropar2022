# Heteropar 2022 Reproductibility

This repository contains scripts that can be used to reproduce the
experiments presented in *Programming Heterogeneous Architectures
Using Hierarchical Tasks*.

## Requirements

| Package    | Version    |
|---------   |---------   |
| GCC        | 10.3.0     |
| Cuda       | 11.2       |
| Hwloc      | 2.7.0      |
| Intel MKL  | 2020.4.304 |
| pkg-config | 0.27.1     |
| CMake      | 3.15.3     |

The experiments were conducted on 2 Intel Xeon Gold 6142 of 16 cores
each and 2 Nvidia V100.

## Installing StarPU

Running the script `starpu_install.sh` will clone, compile and install
two versions of StarPU:

1. The non-hierarchical version, by default built in
   `./starpu/non-h-build/` and installed in `./starpu/non-h-install/`
2. The hierarchical version, by default built in `./starpu/h-build/`
   and installed in `./starpu/h-install`

## Installing Chameleon

Running the script `chameleon_install.sh` will similarly build two
versions of Chameleon:

1. The non-hierarchical version, by default built in
   `./chameleon/non-h-build/`.
2. The hierarchical version, by default built in `./chameleon/h-build/`.

## Running Benchmarks

### GEMM

- Run `gemm_benchmarks.sh` to launch the experiments.
- Run `Rscript studyGEMM.R` to plot the results.

### POTRF, POSV, POINV

- Run `po_benchmarks.sh` to launch the experiments.
- Run `Rscript studyPO.R` to plot the results.
