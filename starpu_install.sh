#!/bin/bash

source env.sh

# Cloning StarPU from tag papers/heteropar22
git clone --depth 1 --branch papers/heteropar22 https://gitlab.inria.fr/starpu/starpu.git
cd starpu/

# Compilation/Installation
./autogen.sh

## Non-Hierarchical Version
mkdir non-h-build non-h-install
cd non-h-build
../configure --enable-fast --enable-cuda --with-hwloc --prefix=$STARPU_NON_H_PATH --disable-build-doc --disable-opencl
make -j && make install
cd ../

## Hierarchical Version
mkdir h-build h-install
cd h-build
../configure --enable-fast --enable-bubble --enable-cuda --with-hwloc --prefix=$STARPU_H_PATH --disable-build-doc --disable-opencl
make -j && make install
cd ../
