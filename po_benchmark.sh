#!/bin/bash

source env.sh

non_h_path=$CHAMELEON_NON_H_BUILD/testing
h_path=$CHAMELEON_H_BUILD/testing
mkdir potrf posv poinv

# Tile sizes
NB1=2880
NB2=960
NB3=320

# Number of iteration
iter=10

# Matrix sizes
N0=5760:57600:2880
N1=63360:97920:5760

# Calibration
export STARPU_CALIBRATE=2

echo "Calibrating kernels..."
for nb in $NB1 $NB2 $NB3
do
    echo "Tile size: $nb"
    $non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o potrf -n $(( $nb * 15 )) -b $nb > /dev/null 2>&1
    $non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o posv  -n $(( $nb * 15 )) -b $nb > /dev/null 2>&1
    $non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o poinv -n $(( $nb * 15 )) -b $nb > /dev/null 2>&1
done
echo "Done"

export STARPU_CALIBRATE=0

for op in potrf posv poinv
do
    # Non-hierarchical
    output=${op}/${op}_2880.csv && echo $output
    $non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o $op --rec diag --rarg 1 -n $N0 -b $NB1 -l $iter >  $output
    $non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o $op --rec diag --rarg 1 -n $N1 -b $NB1 -l $iter >> $output
    sed -i '1!{/Id/d}' $output # Remove the extra header

    output=${op}/${op}_960.csv && echo $output
    $non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o $op --rec diag --rarg 1 -n $N0 -b $NB2 -l $iter >  $output
    $non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o $op --rec diag --rarg 1 -n $N1 -b $NB2 -l $iter >> $output
    sed -i '1!{/Id/d}' $output # Remove the extra header

    output=${op}/${op}_320.csv && echo $output
    $non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o $op --rec diag --rarg 1 -n $N0 -b $NB3 -l $iter >  $output

    # Hierarchical
    output=${op}/rec${op}_1lvl.csv && echo $output
    $h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o rec$op --rec diag --rarg 1 -n $N0 -b $NB1 --l1 $NB2           -l $iter >  $output
    $h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o rec$op --rec diag --rarg 1 -n $N1 -b $NB1 --l1 $NB2           -l $iter >> $output
    sed -i '1!{/Id/d}' $output # Remove the extra header

    output=${op}/rec${op}_2lvl.csv && echo $output
    $h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o rec$op --rec diag --rarg 1 -n $N0 -b $NB1 --l1 $NB2 --l2 $NB3 -l $iter >  $output
    $h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o rec$op --rec diag --rarg 1 -n $N1 -b $NB1 --l1 $NB2 --l2 $NB3 -l $iter >> $output
    sed -i '1!{/Id/d}' $output # Remove the extra header
done
