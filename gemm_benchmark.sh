#!/bin/bash

source env.sh

non_h_path=$CHAMELEON_NON_H_BUILD/testing
h_path=$CHAMELEON_H_BUILD/testing
mkdir gemm

# Tile sizes
NB1=2880
NB2=960
NB3=480
NB4=320
NB5=240

# Number of iteration
iter=10

# Matrix sizes
N1GPU0=5760:23040:2880
N1GPU1=28800:46080:5760

N2GPU0=5760:23040:2880
N2GPU1=28800:57600:5760

# Calibration
export STARPU_CALIBRATE=2

echo "Calibrating kernels..."
for nb in $NB1 $NB2 $NB3 $NB4 $NB5
do
    echo "Tile size: $nb"
    $non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o gemm -n $(( $nb * 10 )) -k $nb -b $nb > /dev/null 2>&1
done
echo "Done"

export STARPU_CALIBRATE=0

# 1 GPU
export STARPU_NCUDA=1

output=gemm/gemm_1gpu_2880.csv && echo $output
$non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 1 -o gemm --rec diag --rarg 1 -n $N1GPU0 -b $NB1 -l $iter >  $output
$non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 1 -o gemm --rec diag --rarg 1 -n $N1GPU1 -b $NB1 -l $iter >> $output
sed -i '1!{/Id/d}' $output # Remove the extra header

output=gemm/gemm_1gpu_960.csv && echo $output
$non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 1 -o gemm --rec diag --rarg 1 -n $N1GPU0 -b $NB2 -l $iter >  $output
$non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 1 -o gemm --rec diag --rarg 1 -n $N1GPU1 -b $NB2 -l $iter >> $output
sed -i '1!{/Id/d}' $output # Remove the extra header

output=gemm/recgemm_1gpu_1lvl.csv && echo $output
$h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 1 -o recgemm --rec diag --rarg 1 -n $N1GPU0 -b $NB1 --l1 $NB2                   -l $iter >  $output
$h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 1 -o recgemm --rec diag --rarg 1 -n $N1GPU1 -b $NB1 --l1 $NB2                   -l $iter >> $output
sed -i '1!{/Id/d}' $output # Remove the extra header

output=gemm/recgemm_1gpu_2lvl.csv && echo $output
$h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 1 -o recgemm --rec diag --rarg 1 -n $N1GPU0 -b $NB1 --l1 $NB2 --l2 320          -l $iter >  $output
$h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 1 -o recgemm --rec diag --rarg 1 -n $N1GPU1 -b $NB1 --l1 $NB2 --l2 320          -l $iter >> $output
sed -i '1!{/Id/d}' $output # Remove the extra header

output=gemm/recgemm_1gpu_3lvl.csv && echo $output
$h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 1 -o recgemm --rec diag --rarg 1 -n $N1GPU0 -b $NB1 --l1 $NB2 --l2 480 --l3 240 -l $iter >  $output
$h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 1 -o recgemm --rec diag --rarg 1 -n $N1GPU1 -b $NB1 --l1 $NB2 --l2 480 --l3 240 -l $iter >> $output
sed -i '1!{/Id/d}' $output # Remove the extra header

# 2 GPU
export STARPU_NCUDA=2

output=gemm/gemm_2gpu_2880.csv && echo $output
$non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o gemm --rec diag --rarg 1 -n $N2GPU0 -b $NB1 -l $iter >  $output
$non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o gemm --rec diag --rarg 1 -n $N2GPU1 -b $NB1 -l $iter >> $output
sed -i '1!{/Id/d}' $output # Remove the extra header

output=gemm/gemm_2gpu_960.csv && echo $output
$non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o gemm --rec diag --rarg 1 -n $N2GPU0 -b $NB2 -l $iter >  $output
$non_h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o gemm --rec diag --rarg 1 -n $N2GPU1 -b $NB2 -l $iter >> $output
sed -i '1!{/Id/d}' $output # Remove the extra header

output=gemm/recgemm_2gpu_1lvl.csv && echo $output
$h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o recgemm --rec diag --rarg 1 -n $N2GPU0 -b $NB1 --l1 $NB2                   -l $iter >  $output
$h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o recgemm --rec diag --rarg 1 -n $N2GPU1 -b $NB1 --l1 $NB2                   -l $iter >> $output
sed -i '1!{/Id/d}' $output # Remove the extra header

output=gemm/recgemm_2gpu_2lvl.csv && echo $output
$h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o recgemm --rec diag --rarg 1 -n $N2GPU0 -b $NB1 --l1 $NB2 --l2 320          -l $iter >  $output
$h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o recgemm --rec diag --rarg 1 -n $N2GPU1 -b $NB1 --l1 $NB2 --l2 320          -l $iter >> $output
sed -i '1!{/Id/d}' $output # Remove the extra header

output=gemm/recgemm_2gpu_3lvl.csv && echo $output
$h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o recgemm --rec diag --rarg 1 -n $N2GPU0 -b $NB1 --l1 $NB2 --l2 480 --l3 240 -l $iter >  $output
$h_path/chameleon_dtesting --mtxfmt=1 -t 30 -g 2 -o recgemm --rec diag --rarg 1 -n $N2GPU1 -b $NB1 --l1 $NB2 --l2 480 --l3 240 -l $iter >> $output
sed -i '1!{/Id/d}' $output # Remove the extra header
